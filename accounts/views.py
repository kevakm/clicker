from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.urls import reverse_lazy
from django.views.generic import DetailView
from django.views.generic import TemplateView

from accounts.models import MyUser


class PickRoleView(TemplateView):
    template_name = "pick_role.html"
    success_url = reverse_lazy('posts:home')

    def get(self, *args):
        if self.request.user.is_clicker or self.request.user.is_company:
            return redirect(reverse_lazy('posts:home'))
        else:
            return super(PickRoleView, self).get(*args)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            role_picked = request.POST.get('role_picked', '')
            if "client" in role_picked:
                user = MyUser.objects.get(id=self.request.user.id)
                user.role = "CLICKER"
                user.save()

            if "company" in role_picked:
                user = MyUser.objects.get(id=self.request.user.id)
                user.role = "COMPANY"
                user.save()
            return HttpResponseRedirect(self.success_url)

        else:
            return super(PickRoleView, self).get(self, request, *args, **kwargs)


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "profile.html"
    model = MyUser

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            action = request.POST.get('action', '')
            user = request.user
            print(action)
            if action == "Deposit":
                amount = request.POST.get('amount', 0)
                user.credit += float(amount)
                user.save()
                messages.success(request, "You have successfully deposited {} credits.".format(amount))
                return JsonResponse({'credit': user.credit})
            elif action == "Withdraw":
                amount = user.credit
                user.credit = 0.0
                user.save()
                messages.success(request, "You have successfully withdrew {} credits.".format(amount))
                return JsonResponse({'credit': user.credit})
        else:
            return super(ProfileView, self).get(self, request, *args, **kwargs)
