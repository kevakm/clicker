from django.conf.urls import url

from accounts.views import PickRoleView, ProfileView

urlpatterns = [
    url(r'registration', PickRoleView.as_view(), name='pick_role'),
    url(r'profile/$', ProfileView.as_view(), name='profile')
]