from django.contrib.auth.mixins import AccessMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect
from django.urls import reverse


class UndefinedRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_undefined:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect(reverse('posts:home'))

        return super(UndefinedRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class ClickerRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_clicker:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect(reverse('posts:home'))

        return super(ClickerRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class CompanyRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_company:
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect(reverse('posts:home'))

        return super(CompanyRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class AuthorRequiredMixin(AccessMixin):

    def authors(self):
        return None

    def dispatch(self, request, *args, **kwargs):
        if not self.authors() or request.user not in self.authors():
            if self.raise_exception:
                raise PermissionDenied  # return a forbidden response
            else:
                return redirect(reverse("posts:home"))

        return super(AuthorRequiredMixin, self).dispatch(request, *args, **kwargs)
