# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-15 09:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20161208_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='myuser',
            name='fb_uid',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
    ]
