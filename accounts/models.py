from allauth.account.signals import user_signed_up
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.contrib.auth.models import AbstractUser
from django_countries import countries
from django_countries.fields import CountryField


class MyUser(AbstractUser):

    roles = (
        ('UNDEFINED', 'Undefined'),
        ('CLICKER', 'Clicker'),
        ('COMPANY', 'Company'),
    )

    gender = (
        ('UNKNOWN', 'Unknown'),
        ('MALE', 'Male'),
        ('FEMALE', 'Female'),
    )

    role = models.CharField(max_length=255, choices=roles, default='UNDEFINED')
    gender = models.CharField(max_length=255, choices=gender, default='UNKNOWN')
    country = CountryField(blank=True, null=True)
    credit = models.FloatField(default=0.0)

    @property
    def is_undefined(self):
        return self.role == 'UNDEFINED'

    @property
    def is_clicker(self):
        return self.role == 'CLICKER'

    @property
    def is_company(self):
        return self.role == 'COMPANY'


@receiver(pre_save, sender=MyUser)
def update_username_from_email(sender, instance, **kwargs):
    instance.username = instance.email


@receiver(user_signed_up)
def set_fb_uid(sender, **kwargs):
    user = kwargs.pop('user')
    extra_data = user.socialaccount_set.filter(provider='facebook')[0].extra_data
    try:
        country = extra_data['location']['location']['country']
    except KeyError:
        country = ""
    gender = extra_data['gender']

    if gender == 'male':
        user.gender = 'MALE'
    elif gender == 'female':
        user.gender = 'FEMALE'
    if country:
        country_code = countries.by_name(country)
        user.country = country_code
    user.save()
