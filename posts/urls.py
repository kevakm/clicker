from django.conf.urls import url

from posts.views import HomeView, CreatePostView, MyPostsView, ChangePostStatusView, CountryAutocomplete, \
    like_share_ajax, DetailPostView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^post/new/$', CreatePostView.as_view(), name='post_create'),
    url(r'^my-posts/$', MyPostsView.as_view(), name='my_posts'),
    url(r'^my-posts/(?P<user_post_id>\w+)/(?P<status>0|1)/$',
        ChangePostStatusView.as_view(), name='status_change'),
    url(r'^country-autocomplete/$', CountryAutocomplete.as_view(), name='country_autocomplete'),
    url(r'^post/(?P<pk>\d+)/$', DetailPostView.as_view(), name='post_detail'),
    url(r'^post/like/$', like_share_ajax, name='like_share')
]