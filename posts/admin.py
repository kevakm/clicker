from django.contrib import admin

from posts.models import Post, Country, SharedPost, LikesExtra, SharesExtra

admin.site.register(Post)
admin.site.register(Country)
admin.site.register(SharedPost)
admin.site.register(LikesExtra)
admin.site.register(SharesExtra)
