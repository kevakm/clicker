import json
import time
import requests
from allauth.socialaccount.models import SocialToken, SocialAccount
import datetime
from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import RedirectView
from django.views.generic import UpdateView
from el_pagination.views import AjaxListView

from accounts.mixins import CompanyRequiredMixin, AuthorRequiredMixin, ClickerRequiredMixin
from posts.forms import PostForm
from posts.models import Post, Country, SharedPost, LikesExtra, SharesExtra


class HomeView(LoginRequiredMixin, AjaxListView):
    template_name = "home.html"
    page_template = "single_post.html"
    model = Post

    def get_queryset(self):
        my_country = self.request.user.country.name
        my_gender = self.request.user.gender
        print(my_gender)
        if my_gender == "UNKNOWN":
            gender = 0
        elif my_gender == "MALE":
            gender = 1
        elif my_gender == "FEMALE":
            gender = 2
        print(gender)
        return Post.objects.filter(post_status=0). \
            filter(Q(countries_targeted__name__contains=my_country) | Q(countries_targeted__isnull=True) & Q(
            gender_targeted=0) | Q(gender_targeted=gender))

    def get(self, *args):
        if self.request.user.is_authenticated:
            if self.request.user.is_undefined:
                return redirect(reverse_lazy('accounts:pick_role'))
            else:
                return super(HomeView, self).get(*args)
        else:
            return super(HomeView, self).get(*args)


class CreatePostView(LoginRequiredMixin, CompanyRequiredMixin, CreateView):
    template_name = "post_create.html"
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('posts:home')

    def get_initial(self):
        return {'my_user': self.request.user}

    def get_form_kwargs(self):
        kwargs = super(CreatePostView, self).get_form_kwargs()
        kwargs.update({'post_user': self.request.user})
        return kwargs


class MyPostsView(LoginRequiredMixin, CompanyRequiredMixin, ListView):
    template_name = "my_posts.html"
    model = Post

    def get_queryset(self):
        return Post.objects.filter(my_user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(MyPostsView, self).get_context_data(**kwargs)
        context['todays_date'] = datetime.datetime.now().date()
        return context


class DetailPostView(LoginRequiredMixin, AuthorRequiredMixin, DetailView):
    template_name = "post_detail.html"
    model = Post

    def authors(self):
        return [self.get_object().my_user]

    def get_context_data(self, **kwargs):
        context = super(DetailPostView, self).get_context_data(**kwargs)
        post = get_object_or_404(Post, id=self.kwargs.get('pk'))
        try:
            start_date = int(post.likesextra_set.first().timestamp)
        except AttributeError:
            start_date = time.time()

        last_date = ""
        likes = []
        like_extras = post.likesextra_set.all()
        for like in like_extras:
            if str(like.date) != last_date:
                likes.append([like.timestamp * 1000, int(like_extras.filter(date=like.date).count())])
                last_date = str(like.date)

        last_date = ""
        shares = []
        share_extras = post.sharesextra_set.all()
        for share in share_extras:
            if str(share.date) != last_date:
                shares.append([share.timestamp *1000, int(share_extras.filter(date=share.date).count())])
                last_date = str(share.date)

        context['start_date'] = start_date
        context['todays_date'] = datetime.datetime.now().date()
        context['likes'] = likes
        context['shares'] = shares
        return context


class ChangePostStatusView(LoginRequiredMixin, CompanyRequiredMixin, RedirectView):
    url = reverse_lazy('posts:my_posts')

    def get(self, request, *args, **kwargs):
        post = get_object_or_404(Post, user_post_id=kwargs.get('user_post_id'))

        if kwargs.get('status') == '0' and post.my_user == self.request.user:
            post.post_status = 1
            post.save()
        elif kwargs.get('status') == '1' and post.my_user == self.request.user and post.my_user.credit > 0:
            post.post_status = 0
            post.save()
        else:
            messages.warning(request, "You need more credit in order to activate your posts.")

        return super().get(request, *args, **kwargs)

    # def get_redirect_url(self, *args, **kwargs):
    #     print(self.request)
    #     post_id = self.kwargs.get('pk')
    #     print(post_id)
    #     if post_id:
    #         print(post_id)
    #     else:
    #         return reverse_lazy('posts:my_posts')


class CountryAutocomplete(autocomplete.Select2QuerySetView):
    def get_result_label(self, item):
        output = u'<img src="http://localhost:8000/static/flags/{0}.gif" ' \
                 u'style="padding-bottom:2px;"> {1}'.format(item.code, item.name)
        return output

    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return Country.objects.none()

        qs = Country.objects.all()

        if self.q:
            qs = qs.filter(name__istartswith=self.q)

        return qs


def like_share_ajax(request):
    if request.method == 'POST' and request.is_ajax():
        post_id = request.POST.get('post_id', '')
        action = request.POST.get('action', '')
        user = request.user
        post = get_object_or_404(Post, id=post_id)

        if action == "share":
            message = request.POST.get('message', '')
            user_friends = user.socialaccount_set.all()[0].extra_data['friends']['summary']['total_count']
            link = post.url
            access_token = SocialToken.objects.get(account__user=user, account__provider='facebook')
            data = {'message': message, 'link': link, 'access_token': access_token}
            r = requests.post('https://graph.facebook.com/me/feed', params=data)
            response = json.loads(r.text)

            try:
                shared_id = response['id']
            except TypeError:
                shared_id = ""
            if shared_id:
                SharedPost(original=post, shared_id=shared_id).save()

            shares_extra = SharesExtra(user=user, post=post, reach=int(user_friends))
            shares_extra.save()
            post.shared_with_friends += user_friends
            post.save()
            post_owner = post.my_user
            post_owner.credit -= float(user_friends / 1000)
            post_owner.save()
            if post_owner.credit <= 0:
                for post in post_owner.post_set.all():
                    post.post_status = 1
                    post.save()

            user.credit += float(user_friends / 1000)
            user.save()
            user_total_credit = user.credit
            user_total_shares = user.shares.count()
            return JsonResponse(
                {'shares_count': post.shares.count(), 'post_id': post_id, 'user_total_shares': user_total_shares,
                 'user_total_credit': user_total_credit})

        if action == "like":
            if post.likes.filter(id=user.id).exists():
                LikesExtra.objects.get(user=user, post=post).delete()
                action = "disliked"
            else:
                likes_extra = LikesExtra(user=user, post=post)
                likes_extra.save()
                action = "liked"
            user_total_likes = user.likes.count()
            return JsonResponse(
                {'likes_count': post.total_likes, 'user_total_likes': user_total_likes, 'action': action})



            # else:
            #     return super(LikeShareView, self).get(self, request, *args, **kwargs)

            # def get(self, *args):
            #     access_token = SocialToken.objects.get(account__user=self.request.user, account__provider='facebook')
            #     social_acc = SocialAccount.objects.get(user=self.request.user)
            #     print(access_token)
            #     print(social_acc.uid)
            #     print(self.request.user)
            #
            #     r = requests.get('https://graph.facebook.com/{0}?access_token={1}'.format(social_acc.uid, access_token))
            #     print(r.url)
            #     print(r.text)
            #     print(r.json())
            #
            #     # data = {'message': 'Hello, World.', 'access_token': access_token}
            #     # r1 = requests.post('https://graph.facebook.com/me/feed', params=data)
            #     # print(r1.text)
            #
            #     return super(HomeView, self).get(*args)
