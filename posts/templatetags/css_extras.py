from django import template
from django.utils.translation import ugettext as _

register = template.Library()


@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class": css})


@register.inclusion_tag('form_field.html')
def render_field(form_field, name, label=None, additional_class=None, label_class=None, icon=None):
    return {'field': {
        'name': name,
        'label': _(label),
        'form_field': form_field,
        'additional_class': additional_class,
        'label_class': label_class,
        'icon': icon
    }}


@register.filter
def divide(value, arg):
    try:
        return int(value) / int(arg)
    except (ValueError, ZeroDivisionError):
        return None


@register.filter
def get_sum(queryset, arg):
    total = 0
    for item in queryset:
        if arg == "likes":
            total += item.likes.count()
        elif arg == "shares":
            total += item.shares.count()
    return total
