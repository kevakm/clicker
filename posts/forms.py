import urllib.parse as urlparse

from dal import autocomplete
from django.core.exceptions import ValidationError
from django.forms import ModelForm, forms, DateInput, DateField, ChoiceField
from django.forms.widgets import SelectDateWidget
from django_countries import countries, widgets


from posts.models import Post


class PostForm(ModelForm):
    post_ends = DateField(widget=DateInput(attrs={'id': 'datepicker'}))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('post_user')
        super(PostForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Post
        fields = ['url', 'post_ends', 'gender_targeted', 'countries_targeted', 'my_user']
        widgets = {
            'countries_targeted': autocomplete.ModelSelect2Multiple(url='posts:country_autocomplete',
                                                                    attrs={'data-html': 'true',
                                                                           'class': 'form-control',
                                                                           'data-placeholder': "leave blank if you want it to be visible to everyone"})
        }

    def clean(self):
        url = self.cleaned_data.get('url')
        if url:
            parsed = urlparse.urlparse(url)
            netloc = parsed.netloc
            if (netloc == "www.facebook.com") and ("story_fbid" and "id") in parsed.query or "videos" in parsed.path or "photo" in parsed.path or "posts" in parsed.path:
                if self.user.credit > 0:
                    return self.cleaned_data
                else:
                    raise forms.ValidationError("You don't have enough credit to promote your post!")
            else:
                raise forms.ValidationError("This is not the right Facebook link!")




