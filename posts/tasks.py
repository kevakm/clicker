from rest_api.celery import app as celery_app


@celery_app.task
def deactivate_post(post_id):
    from posts.models import Post
    post_object = Post.objects.get(id=post_id)
    post_object.post_status = 1
    post_object.save()
