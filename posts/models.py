import time
import datetime
import requests

from allauth.socialaccount.models import SocialToken
from django.db import models

import urllib.parse as urlparse

from django_countries.fields import CountryField
from embed_video.fields import EmbedVideoField

from accounts.models import MyUser
from posts.tasks import deactivate_post

POST_STATUS = (
    (0, 'Active'),
    (1, 'Deactivated')
)

GENDER_TARGETED = (
    (0, 'All'),
    (1, 'Male'),
    (2, 'Female')
)


class Country(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'


class Post(models.Model):
    url = models.CharField(max_length=2000)
    fb_post_id = models.IntegerField(blank=True, null=True)
    fb_user_id = models.IntegerField(blank=True, null=True)
    user_post_id = models.CharField(max_length=255, blank=True, null=True)
    post_starts = models.DateTimeField(default=datetime.datetime.now)
    post_ends = models.DateField(blank=True, null=True)
    post_status = models.IntegerField(default=0, choices=POST_STATUS)

    created_time = models.DateField(blank=True, null=True)
    message = models.CharField(max_length=4000, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    caption = models.CharField(max_length=255, blank=True, null=True)
    picture_url = models.CharField(max_length=2000, blank=True, null=True)
    video_url = EmbedVideoField(blank=True, null=True)
    video_fb = models.URLField(max_length=500, blank=True, null=True)
    link_url = models.CharField(max_length=2000, blank=True, null=True)

    gender_targeted = models.IntegerField(default=0, choices=GENDER_TARGETED)
    countries_targeted = models.ManyToManyField(Country, blank=True, related_name="posts_countries")

    likes = models.ManyToManyField(MyUser, related_name="likes", blank=True, through='LikesExtra')
    shares = models.ManyToManyField(MyUser, related_name="shares", blank=True, through='SharesExtra')
    shared_with_friends = models.PositiveIntegerField(default=0)
    my_user = models.ForeignKey(MyUser)

    def __str__(self):
        return str(self.id)

    @property
    def total_likes(self):
        return self.likes.count()

    @property
    def total_shares(self):
        return self.shares.count()

    def save(self, *args, **kwargs):
        url = self.url
        parsed = urlparse.urlparse(url)
        user = self.my_user
        access_token = SocialToken.objects.get(account__user=user, account__provider='facebook')

        if "videos" in parsed.path:
            split_list = parsed.path.split("/")

            # if "vb" in parsed.path
            if len(split_list) == 6:
                self.fb_post_id = int(split_list[4])
                self.fb_user_id = int(split_list[1])
                self.user_post_id = "_".join([str(self.fb_user_id), str(self.fb_post_id)])
            elif len(split_list) == 5:
                self.fb_post_id = int(split_list[3])
                if split_list[1].isdigit():
                    self.fb_user_id = int(split_list[1])
                    self.user_post_id = "_".join([str(self.fb_user_id), str(self.fb_post_id)])
                else:
                    fb_page_username = str(split_list[1])
                    r = requests.get('https://graph.facebook.com/{0}?access_token={1}'.format(fb_page_username, access_token))
                    fb_user_id = r.json()['id']
                    self.fb_user_id = int(fb_user_id)
                    self.user_post_id = "_".join([fb_user_id, split_list[3]])
        elif "photo.php" in parsed.path:
            self.fb_post_id = int(urlparse.parse_qs(parsed.query)['fbid'][0])
            self.fb_user_id = int(urlparse.parse_qs(parsed.query)['set'][0].split('.')[-1])
            self.user_post_id = "_".join([str(self.fb_user_id), str(self.fb_post_id)])
        elif "photos" in parsed.path:
            self.fb_post_id = int(parsed.path.split('.')[-1].split('/')[1])
            self.fb_user_id = int(parsed.path.split('.')[-1].split('/')[0])
            self.user_post_id = "_".join([str(self.fb_user_id), str(self.fb_post_id)])
        elif "story_fbid" in parsed.query:
            self.fb_post_id = int(urlparse.parse_qs(parsed.query)['story_fbid'][0])
            self.fb_user_id = int(urlparse.parse_qs(parsed.query)['id'][0])
            self.user_post_id = "_".join([str(self.fb_user_id), str(self.fb_post_id)])
        elif "posts" in parsed.path:
            self.fb_post_id = int(parsed.path.split("/")[-1])
            self.fb_user_id = int(user.socialaccount_set.all()[0].uid)
            self.user_post_id = "_".join([str(self.fb_user_id), str(self.fb_post_id)])

        r = requests.get('https://graph.facebook.com/{0}?fields=message,created_time,name,description,caption,attachments,source&'
                         'access_token={1}'.format(self.user_post_id, access_token))
        rjson = r.json()

        if "message" in rjson:
            self.message = rjson['message']
        if "name" in rjson:
            self.name = rjson['name']
        if "description" in rjson:
            self.description = rjson['description']
        if "caption" in rjson:
            self.caption = rjson['caption']
        if "source" in rjson:
            video = rjson['source']
            if "video.xx.fbcdn.net" in video:
                self.video_fb = video
            else:
                self.video_url = video
        if "attachments" in rjson:
            self.picture_url = rjson['attachments']['data'][0]['media']['image']['src']
        if "link" in rjson:
            self.link_url = rjson['link']

        create_task = False
        if self.id is None:
            create_task = True

        super(Post, self).save(*args, **kwargs)

        if create_task:
            post_starts = self.post_starts
            post_ends = self.post_ends
            date_range = post_ends - post_starts.date()
            final_date = post_starts + date_range
            deactivate_post.apply_async(args=[self.id], eta=final_date)

    class Meta:
        ordering = ["-post_starts"]


class SharedPost(models.Model):
    original = models.ForeignKey(Post, blank=True, related_name="original_post")
    shared_id = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.shared_id


class LikesExtra(models.Model):
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateField(default=datetime.date.today)
    timestamp = models.IntegerField(default=int(time.time()))

    def __str__(self):
        return str(self.post.id)


class SharesExtra(models.Model):
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateField(default=datetime.date.today)
    timestamp = models.IntegerField(default=int(time.time()))
    reach = models.PositiveIntegerField(default=0)

    def __str__(self):
        return str(self.post.id)
